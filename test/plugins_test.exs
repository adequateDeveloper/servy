defmodule Servy.PluginsTest do

  use ExUnit.Case, async: true
  import ExUnit.CaptureLog
  alias Servy.{Conversation, Plugins}

  @input %Conversation{method: "GET", params: %{}, path: "", response_body: "",
                       status: 0}

  test "rewrite path '/wildlife' to '/wildthing' returns correct response Conversation" do
    input = %{@input | path: "/wildlife"}

    expected = %Conversation{method: "GET", path: "/wildthings",
                                          status: 0, response_body: ""}
    assert Plugins.rewrite_path(input) == expected
  end

  test "track_404_response generates status: 404 log message" do
    input = %{@input | path: "/bigfoot", response_body: "No /bigfoot here!",
                       status: 404}
                       
    fun = fn -> Plugins.track_404_response(input) end
    assert capture_log(fun) =~ "[debug] Elixir.Servy.Plugins.track_404_response(conversation):"
  end

end
