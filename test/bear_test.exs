defmodule Servy.BearTest do
  use ExUnit.Case, async: true
  alias Servy.{Bear, Wildthings}
  doctest Bear

  test "is_grizzly? returns true" do
      Wildthings.get_bear(4)
      |> Bear.is_grizzly?
  end

  test "is_grizzly? returns false" do
      Wildthings.get_bear(3)
      |> Bear.is_grizzly?
  end

end
