defmodule Servy.BearControllerTest do

  use ExUnit.Case, async: true
  alias Servy.{BearController, Conversation}

  @input %Conversation{method: "GET", headers: %{}, params: %{}, path: "",
                       response_body: "", status: 0}

  # @expected_body "<ul><li>Brutus - Grizzly</li><li>Iceman - Polar</li><li>Kenai - Grizzly</li><li>Paddington - Brown</li><li>Roscoe - Panda</li><li>Rosie - Black</li><li>Scarface - Grizzly</li><li>Smokey - Black</li><li>Snow - Polar</li><li>Teddy - Brown</li></ul>"

  @expected_body "<h1>All The Bears!</h1>\n\n<ul>\n  \n    <li>Brutus - Grizzly</li>\n  \n    <li>Iceman - Polar</li>\n  \n    <li>Kenai - Grizzly</li>\n  \n    <li>Paddington - Brown</li>\n  \n    <li>Roscoe - Panda</li>\n  \n    <li>Rosie - Black</li>\n  \n    <li>Scarface - Grizzly</li>\n  \n    <li>Smokey - Black</li>\n  \n    <li>Snow - Polar</li>\n  \n    <li>Teddy - Brown</li>\n    \n</ul>\n\n[%Servy.Bear{hibernating: false, id: 6, name: \"Brutus\", type: \"Grizzly\"}, %Servy.Bear{hibernating: true, id: 9, name: \"Iceman\", type: \"Polar\"}, %Servy.Bear{hibernating: false, id: 10, name: \"Kenai\", type: \"Grizzly\"}, %Servy.Bear{hibernating: false, id: 3, name: \"Paddington\", type: \"Brown\"}, %Servy.Bear{hibernating: false, id: 8, name: \"Roscoe\", type: \"Panda\"}, %Servy.Bear{hibernating: true, id: 7, name: \"Rosie\", type: \"Black\"}, %Servy.Bear{hibernating: true, id: 4, name: \"Scarface\", type: \"Grizzly\"}, %Servy.Bear{hibernating: false, id: 2, name: \"Smokey\", type: \"Black\"}, %Servy.Bear{hibernating: false, id: 5, name: \"Snow\", type: \"Polar\"}, %Servy.Bear{hibernating: true, id: 1, name: \"Teddy\", type: \"Brown\"}]\n"

  test "index for '/bears' returns correct response conversation body" do
    input = %{@input | path: "/bears"}
    expected = %Conversation{method: "GET", path: "/bears",
                             response_body: @expected_body, status: 200}
    assert BearController.index(input) == expected
  end

  test "index for '/grizzly' returns correct response conversation body" do
    input = %{@input | path: "/grizzly"}
    expected_body = "<ul><li>Brutus - Grizzly</li><li>Kenai - Grizzly</li><li>Scarface - Grizzly</li></ul>"
    expected = %Conversation{method: "GET", path: "/grizzly",
                             response_body: expected_body, status: 200}
    assert BearController.index_grizzly(input) == expected
  end

  test "create POST '/bears' input returns correct response conversation body" do
    params = %{"name" => "Baloo", "type" => "Brown"}
    input = %{@input | method: "POST", params: params, path: "/bears"}
    expected = %Conversation{method: "POST", params: params, path: "/bears",
                             response_body: "Created a Brown bear named Baloo!", status: 201}
    assert BearController.create(input, params) == expected
  end

  test "show '/bears/1' input returns correct response conversation body" do
    params = Map.put(@input.params, "id", 1)
    input = %{@input | path: "/bears/1"}
    expected_body = "<h1>Show Bears</h1>\n<p>\nIs Teddy hibernating? <strong>true</strong>\n</p>\n"
    expected = %Conversation{method: "GET", path: "/bears/1",
                             response_body: expected_body, status: 200}
    assert BearController.show(input, params) == expected
  end

  test "delete '/bears/1' input returns correct response conversation body" do
    input = %{@input | method: "DELETE", path: "/bears/1"}
    expected = %Conversation{method: "DELETE", path: "/bears/1",
                             response_body: "Deleting a bear is forbidden!", status: 403}
    assert BearController.delete(input) == expected
  end

end
