defmodule Servy.WildthingsTest do
  use ExUnit.Case, async: true
  alias Servy.{Bear, Wildthings}
  doctest Wildthings

  test "get bear by integer id" do
    expected = %Bear{hibernating: false, id: 3, name: "Paddington", type: "Brown"}
    assert Wildthings.get_bear(3) == expected
  end

  test "get bear by binary id" do
    expected = %Bear{hibernating: false, id: 3, name: "Paddington", type: "Brown"}
    assert Wildthings.get_bear("3") == expected
  end
end
