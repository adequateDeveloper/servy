defmodule Servy.ParserTest do

  use ExUnit.Case, async: true
  alias Servy.{Conversation, Parser}

  # The HTTP spec requires each line to end in '\r\n'. Heredoc automatically
  # provides the '\n' so the '\r' must be explicity added.

  @request_header """
  Host: example.com\r
  User-Agent: ExampleBrowser/1.0\r
  Accept: */*\r
  \r
  """

  test "parse request '/wildthings' returns Conversation with empty response body and status = 0" do
    request = """
    GET /wildthings HTTP/1.1\r
    """ <> @request_header

    expected =
      %Conversation{
        method: "GET",
        headers: %{"Accept" => "*/*", "Host" => "example.com", "User-Agent" => "ExampleBrowser/1.0"},
        params: %{}, path: "/wildthings",
        response_body: "", status: 0}

    assert Parser.parse(request) == expected
  end

  test "parse_headers parses a list of header fields into a map" do
    header_lines = [
      "Host: example.com",
      "User-Agent: ExampleBrowser/1.0",
      "Accept: */*",
      "Content-Type: application/x-www-form-urlencoded",
      "Content-Length: 21"
    ]

    expected_headers = %{
      "Host" => "example.com",
      "User-Agent" => "ExampleBrowser/1.0",
      "Accept" => "*/*",
      "Content-Type" => "application/x-www-form-urlencoded",
      "Content-Length" => "21"
    }

    actual_headers = Parser.parse_headers(header_lines, %{})
    assert actual_headers == expected_headers
  end

  test "parse body with content type of form-urlencoded returns map of contents" do
    input = "name=Baloo&type=Brown\n"
    content_type = "application/x-www-form-urlencoded"
    expected = %{"name" => "Baloo", "type" => "Brown"}

    assert expected == Parser.parse_body(content_type, input)
  end

  test "parse body with content type of not form-urlencoded returns empty content map" do
    input = "name=Baloo&type=Brown\n"
    content_type = "multipart/form-data"
    expected = %{}

    assert expected == Parser.parse_body(content_type, input)
  end

end
