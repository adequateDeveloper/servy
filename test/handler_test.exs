defmodule Servy.HandlerTest do

  use ExUnit.Case, async: true
  alias Servy.{Conversation, Handler}

  @input %Conversation{method: "GET", headers: %{}, params: %{}, path: "",
                       response_body: "", status: 0}

  # The HTTP spec requires each line to end in '\r\n'. Heredoc automatically
  # provides the '\n' so the '\r' must be explicity added.

  @request_header """
  Host: example.com\r
  User-Agent: ExampleBrowser/1.0\r
  Accept: */*\r
  \r
  """

  @response_200_header """
  HTTP/1.1 200 OK\r
  Content-Type: text/html\r
  """

  @response_201_header """
  HTTP/1.1 201 Created\r
  Content-Type: text/html\r
  """

  @response_403_header """
  HTTP/1.1 403 Forbidden\r
  Content-Type: text/html\r
  """

  @response_404_header """
  HTTP/1.1 404 Not Found\r
  Content-Type: text/html\r
  """

  @form_body """
  <form action="/bears" method="POST">
    <p>
      Name:<br/>
      <input type="text" name="name">
    </p>
    <p>
      Type:<br/>
      <input type="text" name="type">
    </p>
    <p>
      <input type="submit" value="Create Bear">
    </p>
  </form>
  """

  @about_body """
  <h1>Clark's Wildthings Refuge</h1>

  <blockquote>
    When we contemplate the whole globe as one great dewdrop,
    striped and dotted with continents and islands, flying through
    space with other starts all singing and shining together as one,
    the whole univers appears as an infinite storm of beauty.
    -- John Muir
  </blockquote>
  """

  # @expected_body "<ul><li>Brutus - Grizzly</li><li>Iceman - Polar</li><li>Kenai - Grizzly</li><li>Paddington - Brown</li><li>Roscoe - Panda</li><li>Rosie - Black</li><li>Scarface - Grizzly</li><li>Smokey - Black</li><li>Snow - Polar</li><li>Teddy - Brown</li></ul>"

  @expected_body "<h1>All The Bears!</h1>\n\n<ul>\n  \n    <li>Brutus - Grizzly</li>\n  \n    <li>Iceman - Polar</li>\n  \n    <li>Kenai - Grizzly</li>\n  \n    <li>Paddington - Brown</li>\n  \n    <li>Roscoe - Panda</li>\n  \n    <li>Rosie - Black</li>\n  \n    <li>Scarface - Grizzly</li>\n  \n    <li>Smokey - Black</li>\n  \n    <li>Snow - Polar</li>\n  \n    <li>Teddy - Brown</li>\n    \n</ul>\n\n[%Servy.Bear{hibernating: false, id: 6, name: \"Brutus\", type: \"Grizzly\"}, %Servy.Bear{hibernating: true, id: 9, name: \"Iceman\", type: \"Polar\"}, %Servy.Bear{hibernating: false, id: 10, name: \"Kenai\", type: \"Grizzly\"}, %Servy.Bear{hibernating: false, id: 3, name: \"Paddington\", type: \"Brown\"}, %Servy.Bear{hibernating: false, id: 8, name: \"Roscoe\", type: \"Panda\"}, %Servy.Bear{hibernating: true, id: 7, name: \"Rosie\", type: \"Black\"}, %Servy.Bear{hibernating: true, id: 4, name: \"Scarface\", type: \"Grizzly\"}, %Servy.Bear{hibernating: false, id: 2, name: \"Smokey\", type: \"Black\"}, %Servy.Bear{hibernating: false, id: 5, name: \"Snow\", type: \"Polar\"}, %Servy.Bear{hibernating: true, id: 1, name: \"Teddy\", type: \"Brown\"}]\n"


  test "handle GET request '/wildthings' returns correct response" do
    request =
      """
      GET /wildthings HTTP/1.1\r
      """ <> @request_header

    expected_response =
      @response_200_header <>
      """
      Content-Length: 20\r
      \r
      Bears, Lions, Tigers
      """

    assert Handler.handle(request) == expected_response
  end

  test "handle GET request '/wildlife' returns correct response" do
    request =
      """
      GET /wildlife HTTP/1.1\r
      """ <> @request_header

    expected_response =
      @response_200_header <>
      """
      Content-Length: 20\r
      \r
      Bears, Lions, Tigers
      """

    assert Handler.handle(request) == expected_response
  end

  test "handle GET request '/bears' returns correct response" do
    request =
      """
      GET /bears HTTP/1.1\r
      """ <> @request_header

    expected_response =
      @response_200_header <>
      """
      Content-Length: 1074\r
      \r
      #{@expected_body}
      """

    assert Handler.handle(request) == expected_response
  end

  test "handle POST request '/bears' returns correct response" do
    request =
    """
    POST /bears HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    Content-Type: application/x-www-form-urlencoded\r
    Content-Length: 21\r
    \r
    name=Baloo&type=Brown
    """

    expected_response =
      @response_201_header <>
      """
      Content-Length: 33\r
      \r
      Created a Brown bear named Baloo!
      """

    assert Handler.handle(request) == expected_response
  end

  test "handle request '/bears/1' returns correct response" do
    request =
      """
      GET /bears/1 HTTP/1.1\r
      """ <> @request_header

    expected_response =
      @response_200_header <>
      """
      Content-Length: 73\r
      \r
      <h1>Show Bears</h1>\n<p>\nIs Teddy hibernating? <strong>true</strong>\n</p>\n
      """

    assert Handler.handle(request) == expected_response
  end

  test "handle request '/bigfoot' returns correct response" do
    request =
      """
      GET /bigfoot HTTP/1.1\r
      """ <> @request_header

    expected_response =
      @response_404_header <>
      """
      Content-Length: 17\r
      \r
      No /bigfoot here!
      """

    assert Handler.handle(request) == expected_response
  end

  test "handle request DELETE '/bears' returns correct response" do
    request =
      """
      DELETE /bears/1 HTTP/1.1\r
      """ <> @request_header

    expected_response =
      @response_403_header <>
      """
      Content-Length: 29\r
      \r
      Deleting a bear is forbidden!
      """

    assert Handler.handle(request) == expected_response
  end

  test "handle request '/about' returns correct response" do
    request =
      """
      GET /about HTTP/1.1\r
      """ <> @request_header

    expected_response =
      @response_200_header <>
      """
      Content-Length: 330\r
      \r
      """ <> @about_body <> "\n"

    assert Handler.handle(request) == expected_response
  end

  test "route GET '/wildthings' input returns correct response conversation" do
    input = %{@input | path: "/wildthings"}
    expected = %Conversation{method: "GET", path: "/wildthings",
                             status: 200, response_body: "Bears, Lions, Tigers"}
    assert Handler.route(input) == expected
  end

  test "route GET '/bears/new' input returns correct response conversation" do
    input = %{@input | path: "/bears/new"}
    expected = %Conversation{method: "GET", path: "/bears/new",
                             response_body: @form_body, status: 200}
    assert Handler.route(input) == expected
  end

  test "route GET '/bears' input returns correct response conversation body" do
    input = %{@input | path: "/bears"}
    expected = %Conversation{method: "GET", path: "/bears",
                             response_body: @expected_body, status: 200}
    assert Handler.route(input) == expected
  end

  test "route GET '/grizzly' input returns correct response conversation body" do
    input = %{@input | path: "/grizzly"}
    expected_body = "<ul><li>Brutus - Grizzly</li><li>Kenai - Grizzly</li><li>Scarface - Grizzly</li></ul>"
    expected = %Conversation{method: "GET", path: "/grizzly",
                             response_body: expected_body, status: 200}
    assert Handler.route(input) == expected
  end

  test "route POST '/bears' input returns correct response conversation body" do
    params = %{"name" => "Baloo", "type" => "Brown"}
    input = %{@input | method: "POST", params: params, path: "/bears"}
    expected = %Conversation{method: "POST", params: params, path: "/bears",
                             response_body: "Created a Brown bear named Baloo!", status: 201}
    assert Handler.route(input) == expected
  end

  test "route GET '/bears/1' input returns correct response conversation" do
    input = %{@input | path: "/bears/1"}
    expected_body = "<h1>Show Bears</h1>\n<p>\nIs Teddy hibernating? <strong>true</strong>\n</p>\n"
    expected = %Conversation{method: "GET", path: "/bears/1",
                             response_body: expected_body, status: 200}
    assert Handler.route(input) == expected
  end

  test "route DELETE '/bears/1' input returns correct response conversation" do
    input = %{@input | method: "DELETE", path: "/bears/1"}
    expected = %Conversation{method: "DELETE", path: "/bears/1",
                             response_body: "Deleting a bear is forbidden!", status: 403}
    assert Handler.route(input) == expected
  end

  test "route GET '/bigfoot' input returns correct response conversation" do
    input = %{@input | path: "/bigfoot"}
    expected = %Conversation{method: "GET", path: "/bigfoot",
                             response_body: "No /bigfoot here!", status: 404}
    assert Handler.route(input) == expected
  end

  test "route GET '/about' input returns correct response conversation" do
    input = %{@input | path: "/about"}
    expected = %Conversation{method: "GET", path: "/about",
                             response_body: @about_body, status: 200}
    assert Handler.route(input) == expected
  end

  test "handle_file with correct input returns status: 200 & correct response conversation" do
    page_body = """
    test line 1
    test line 2
    """
    input = %{@input | path: "/about"}
    expected = %Conversation{method: "GET", params: %{}, path: "/about",
                            response_body: page_body, status: 200}

    assert Handler.handle_file({:ok, page_body}, input) == expected
  end

  test "handle_file with :enoent input returns status: 404 & correct response conversation" do
    input = %{@input | path: "/about"}
    expected = %Conversation{method: "GET", params: %{}, path: "/about",
                             response_body: "File not found!", status: 404}

    assert Handler.handle_file({:error, :enoent}, input) == expected
  end

  test "handle_file with :errors input returns status: 500 & correct response conversation" do
    error_reason = "test"
    input = %{@input | path: "/about"}
    expected = %Conversation{method: "GET", params: %{}, path: "/about",
                             response_body: "File error: #{error_reason}", status: 500}

    assert Handler.handle_file({:error, error_reason}, input) == expected
  end

  test "format_response for '/wildthings' input returns valid HTTP response" do
    input = %{@input | path: "/wildthings",
                       response_body: "Bears, Lions, Tigers", status: 200}
    expected_response =
      @response_200_header <>
      """
      Content-Length: 20\r
      \r
      Bears, Lions, Tigers
      """

    assert Handler.format_response(input) == expected_response
  end

  test "format_response for '/bears' input returns valid HTTP response" do
    input = %{@input | path: "/bears",
                       response_body: "Teddy, Smokey, Paddington", status: 200}

    expected_response =
      @response_200_header <>
      """
      Content-Length: 25\r
      \r
      Teddy, Smokey, Paddington
      """

    assert Handler.format_response(input) == expected_response
  end

  test "format_response for '/api/bears' input returns valid JSON response" do
    request = """
    GET /api/bears HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    expected_response = """
    HTTP/1.1 200 OK\r
    Content-Type: application/json\r
    Content-Length: 605\r
    \r
    [{"type":"Brown","name":"Teddy","id":1,"hibernating":true},
     {"type":"Black","name":"Smokey","id":2,"hibernating":false},
     {"type":"Brown","name":"Paddington","id":3,"hibernating":false},
     {"type":"Grizzly","name":"Scarface","id":4,"hibernating":true},
     {"type":"Polar","name":"Snow","id":5,"hibernating":false},
     {"type":"Grizzly","name":"Brutus","id":6,"hibernating":false},
     {"type":"Black","name":"Rosie","id":7,"hibernating":true},
     {"type":"Panda","name":"Roscoe","id":8,"hibernating":false},
     {"type":"Polar","name":"Iceman","id":9,"hibernating":true},
     {"type":"Grizzly","name":"Kenai","id":10,"hibernating":false}]
    """

    response = Handler.handle(request)
    assert remove_whitespace(response) == remove_whitespace(expected_response)
  end

  test "format_response for '/bears/1' input returns valid HTTP response" do
    input = %{@input | path: "/bears",
                       response_body: "Bear 1", status: 200}

    expected_response =
      @response_200_header <>
      """
      Content-Length: 6\r
      \r
      Bear 1
      """

    assert Handler.format_response(input) == expected_response
  end

  test "format_response for '/bigfoot' input returns valid HTTP response" do
    input = %{@input | path: "/bigfoot",
                       response_body: "No /bigfoot here!", status: 404}

    expected_response =
      @response_404_header <>
      """
      Content-Length: 17\r
      \r
      No /bigfoot here!
      """

    assert Handler.format_response(input) == expected_response
  end

  test "format_response for '/bears/new' input returns valid HTTP response" do
    input = %{@input | path: "/bears/new",
                       response_body: @form_body, status: 200}

    expected_response =
      @response_200_header <>
      """
      Content-Length: 232\r
      \r
      """ <> @form_body <> "\n"

    assert Handler.format_response(input) == expected_response
  end

  defp remove_whitespace(text) do
    String.replace(text, ~r{\s}, "")
  end

end
