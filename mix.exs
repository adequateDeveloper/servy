defmodule Servy.Mixfile do
  use Mix.Project

  def project do
    [app: :servy,
     description: "A humble HTTP server",
     version: "0.1.0",
     elixir: "~> 1.5",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    # Specify extra applications you'll use from Erlang/Elixir
    [
      extra_applications: [:logger],
      mod: {Servy, []},
      env: [port: 3000]
    ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:my_dep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:my_dep, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      { :poison, "~> 3.1" },
      { :httpoison, "~> 0.12.0" },
      { :ex_doc, "~> 0.16" },
      { :credo, "~> 0.8" },
      { :dialyxir, "~> 0.5", only: [:dev], runtime: false }
    ]
  end
end
