defmodule Servy do
  @moduledoc """
  Documentation for Servy.
  """

  alias Servy.Supervisor

  use Application

  def start(_type, _args) do
    IO.puts "Starting the application..."
    Supervisor.start_link()
  end
end
