defmodule Servy.KickStarter do
  @moduledoc false

  use GenServer

  alias Servy.HttpServer


  # client api

  def start do
    IO.puts "Starting the kickstarter..."
    GenServer.start __MODULE__, :ok, name: __MODULE__
  end

 def start_link(_arg) do
    IO.puts "Starting the kickstarter..."
    GenServer.start_link __MODULE__, :ok, name: __MODULE__
  end


  # callbacks

  def init(:ok) do
    Process.flag :trap_exit, true
    {:ok, start_server()}
  end

  def handle_info({:EXIT, _pid, reason}, _state) do
    IO.puts "HttpServer exited (#{inspect reason})"

    {:noreply, start_server()}
  end


  # callback helpers

  defp start_server do
    IO.puts "Starting the HTTP Server..."
    port = Application.get_env :servy, :port
    http_pid = spawn_link HttpServer, :start, [port]
    Process.register http_pid, :http_server
    http_pid
  end
  
end

# example
# $ iex -S mix
# iex> alias Servy.KickStarter
# iex> {:ok, kick_pid} = KickStarter.start
# iex> http_pid = Process.whereis :http_server
# iex> Process.info kick_pid, :links
# iex> Process.info http_pid, :links
# iex> Process.exit http_pid, :kaboom
# iex> Process.alive? http_pid
# iex> Process.alive? kick_pid
