defmodule Servy.Bear do
  @moduledoc """
  Represents a Bear internal structure

  Example

      iex> alias Servy.Bear
      Servy.Bear
      iex> bear = %Bear{}
      %Servy.Bear{hibernating: false, id: nil, name: "", type: ""}

  """

  alias Servy.Bear

  defstruct [id: nil,
             name: "",
             type: "",
             hibernating: false]

  @doc """
  Is the bear a Grizzly?
  """
  def is_grizzly?(%Bear{} = bear) do
    bear.type == "Grizzly"
  end

  @doc """
  Order bears by name in ascending order
  """
  def order_asc_by_name(bear1, bear2) do
    bear1.name <= bear2.name
  end

end
