defmodule Servy.PledgeController do
  @moduledoc false

  alias Servy.PledgeServer

  require IO
  require Logger

  @module __MODULE__

  # iex -S mix
  # iex> Servy.PledgeServer.start
  # iex> Servy.HttpServer.start 4000
  #
  # new terminal:
  # curl http://localhost:4000/pledges
  # curl -XPOST http://localhost:4000/pledges -d 'name=larry&amount=100'
  # curl -XPOST http://localhost:4000/pledges -d 'name=moe&amount=200'
  # curl -XPOST http://localhost:4000/pledges -d 'name=curly&amount=300'
  # curl -XPOST http://localhost:4000/pledges -d 'name=daisy&amount=400'
  # curl -XPOST http://localhost:4000/pledges -d 'name=grace&amount=500'
  # curl http://localhost:4000/pledges

  @doc """
  GET recent pledges from the cache
  """
  def index(conversation) do
    pledges = PledgeServer.recent_pledges()

    Logger.debug fn -> "#{@module}.index(conversation): #{inspect conversation}" end

    %{conversation | status: 200, response_body: (inspect pledges)}
  end

  @doc """
  Send the pledge to the external service and cache it
  """
  def create(conversation, %{"name" => name, "amount" => amount} = _params) do
    PledgeServer.create_pledge(name, String.to_integer(amount))

    %{conversation | status: 201, response_body: "#{name} pledged #{amount}!"}
  end

end