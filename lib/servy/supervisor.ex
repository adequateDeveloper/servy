defmodule Servy.Supervisor do
  @moduledoc false
  
  use Supervisor

  alias Servy.{KickStarter, ServicesSupervisor}


  # client api

  def start_link do
    IO.puts "Starting the top level supervisor..."

    Supervisor.start_link __MODULE__, :ok, name: __MODULE__
  end


  # callbacks

  def init(:ok) do
    children = [
      KickStarter,
      ServicesSupervisor,
    ]

    Supervisor.init children, strategy: :one_for_one
  end

end


# example
# $ iex -S mix
# iex> alias Servy.Supervisor
# iex> {:ok, sup_pid} = Supervisor.start_link
# iex> Supervisor.which_children sup_pid
# iex> Supervisor.count_children sup_pid
# iex> Process.whereis(:sensor_server) |> Process.exit(:kaboom)
# iex> Process.whereis(:pledge_server) |> Process.exit(:kaboom)
# iex> Process.whereis(:http_server) |> Process.exit(:kaboom)
# iex> Servy.PledgeServer.child_spec []
# iex> Servy.KickStarter.child_spec []
# iex> Servy.ServicesSupervisor.child_spec []
# iex> pid = Process.whereis Servy.ServicesSupervisor
# iex> Process.exit pid, :kill

