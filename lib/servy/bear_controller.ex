defmodule Servy.BearController do
  @moduledoc """
  Controller for bears requests
  """

  alias Servy.{Bear, Wildthings}

  require IO
  require Logger

  @module __MODULE__

  @templates_path Path.expand("templates", File.cwd!)
  # @templates_path Path.expand("../../templates", __DIR__)

  @doc """
  GET /bears in sorted order
  """
  def index(conversation) do
    bears =
      Wildthings.list_bears
      |> Enum.sort(&Bear.order_asc_by_name/2)

    render(conversation, "index.eex", bears: bears)
  end

  # def index(conversation) do
  #   items =
  #     Wildthings.list_bears
  #     # |> Enum.sort(fn(item1, item2) -> Bear.order_asc_by_name(item1, item2) end)
  #     # |> Enum.sort(&Bear.order_asc_by_name(&1, &2))
  #     |> Enum.sort(&Bear.order_asc_by_name/2)
  #     |> Enum.map(fn(item) -> bear_line_item(item)  end)
  #     |> Enum.join
  #
  #   %{conversation | status: 200, response_body: "<ul>#{items}</ul>"}
  # end

  @doc """
  GET /bears grizzly in sorted order
  """
  def index_grizzly(conversation) do
    items =
      Wildthings.list_bears
      # |> Enum.filter(fn(item) -> Bear.is_grizzly?(item) end)
      # |> Enum.sort(fn(item1, item2) -> Bear.order_asc_by_name(item1, item2) end)
      # |> Enum.filter(&Bear.is_grizzly?(&1))
      # |> Enum.sort(&Bear.order_asc_by_name(&1, &2))
      |> Enum.filter(&Bear.is_grizzly?/1)
      |> Enum.sort(&Bear.order_asc_by_name/2)
      |> Enum.map(fn(item) -> bear_line_item(item)  end)
      |> Enum.join

    %{conversation | status: 200, response_body: "<ul>#{items}</ul>"}
  end

  @doc """
  GET /bears/id
  """
  def show(conversation, %{"id" => id}) do
    bear = Wildthings.get_bear id
    render(conversation, "show.eex", bear: bear)
  end

  @doc """
  POST /bears
  """
  def create(conversation, %{"name" => name, "type" => type} = _params) do
    %{conversation | status: 201, response_body: "Created a #{type} bear named #{name}!"}
  end

  def delete(conversation) do
    %{conversation | status: 403, response_body: "Deleting a bear is forbidden!"}
  end

  defp bear_line_item(item) do
    "<li>#{item.name} - #{item.type}</li>"
  end

  defp render(conversation, template, bindings) do
    content =
      @templates_path
      |> Path.join(template)
      |> EEx.eval_file(bindings)

    Logger.debug fn -> "#{@module}.render(conversation, template, bindings): #{inspect content}" end

    %{conversation | status: 200, response_body: content}
  end

end
