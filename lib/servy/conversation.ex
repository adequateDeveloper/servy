defmodule Servy.Conversation do
  @moduledoc """
  Represents the internal request & response structure

  Example

      iex> alias Servy.Conversation
      Servy.Conversation
      iex> conversation = %Conversation{}
      %Servy.Conversation{method: "", params: %{}, path: "", response_body: "", status: nil}
      iex> Conversation.full_status conversation
      " "
      iex> conversation = %{conversation | status: 200}
      %Servy.Conversation{method: "", params: %{}, path: "", response_body: "", status: 200}
      iex> Conversation.full_status conversation
      "200 OK"

  """

  alias Servy.Conversation

  # A replacement for the previously used map:
  #   @type request_response :: %{method: String.t, path: String.t,
  #                               status: Integer.t, response_body: String.t}

  @status_reason %{
    200 => "OK",
    201 => "Created",
    401 => "Unauthorized",
    403 => "Forbidden",
    404 => "Not Found",
    500 => "Internal Server Error"
  }

  defstruct [method: "",
             params: %{},
             path: "",
             headers: %{},
             response_content_type: "text/html",
             response_body: "",
             status: nil]

  @doc """
  provide the full HTTP status of the Conversation
  """
  @spec full_status(%Conversation{}) :: String.t
  def full_status(%Conversation{} = conversation), do:
    "#{conversation.status} #{@status_reason[conversation.status]}"

end
