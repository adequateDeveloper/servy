defmodule Servy.Fetcher do
  @moduledoc false

  # manual implementation of the Elixir Task

#  def async(camera_name) do
  def async(fun) do
    caller = self()
#    spawn(fn -> send(caller, {:result, VideoCam.get_snapshot(camera_name)}) end)
#    spawn(fn -> send(caller, {:result, fun.()}) end)
    spawn(fn -> send(caller, {self(), :result, fun.()}) end)
  end

#  def get_result do
  def get_result(pid) do
#    receive do {:result, filename} -> filename end
#    receive do {:result, value} -> value end
    receive do {^pid, :result, value} -> value end
  end


end
