defmodule Servy.HttpServer do
  @moduledoc false

  alias Servy.Handler

  # Copied from http://erlang.org/doc/man/gen_tcp.html
  #  server() ->
  #      {ok, LSock} = gen_tcp:listen(5678, [binary, {packet, 0},
  #                                          {active, false}]),
  #      {ok, Sock} = gen_tcp:accept(LSock),
  #      {ok, Bin} = do_recv(Sock, []),
  #      ok = gen_tcp:close(Sock),
  #      Bin.


  # Converted to the elixir equivilent
  # $ iex -S mix
  # iex> Servy.HttpServer.server
  # from browser: localhost:5678
  def server do
    # create the server listening socket
#    {:ok, lsock} = :gen_tcp.listen(5678, [:binary, {:packet, 0}, {:active, false}]),
    {:ok, lsock} = :gen_tcp.listen(5678, [:binary, packet: 0, active: false])

    # blocking call to accept a client connection
    {:ok, sock} = :gen_tcp.accept(lsock)

    # receive all the available bytes of the client request on the client socket
    {:ok, bin} = :gen_tcp.recv(sock, 0)

    # close the client socket
    :ok = :gen_tcp.close(sock)

    # return the request bytes
    # example: "GET / HTTP/1.1\r\nHost: localhost:5678\r\nConnection: keep-alive\r\nCache-Control: max-age=0\r\n
    # Upgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)
    # Ubuntu Chromium/59.0.3071.109 Chrome/59.0.3071.109 Safari/537.36\r\nAccept: text/html,application/xhtml+xml,
    # application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language:
    # en-US,en;q=0.8,ru;q=0.6,es;q=0.4,ko;q=0.2\r\n\r\n"
    bin
  end

  @doc """
  Starts the server on the given `port` of localhost.
  """
  # $ iex -S mix
  # iex> Servy.HttpServer.start 4000
  # from browser: localhost:4000/bears
  # curl http://localhost:4000/api/bears
  def start(port) when is_integer(port) and port > 1023 do

    # Creates a socket to listen for client connections. `listen_socket` is bound to the listening socket.
    # Socket options:
    # `:binary`         - open the socket in "binary" mode and deliver data as binaries
    # `packet: :raw`    - deliver the entire binary without doing any packet handling
    # `active: false`   - receive data when we're ready by calling `gen_tcp.recv/2`
    # `reuseaddr: true` - allows reusing the address if the listener crashes
    {:ok, listen_socket} = :gen_tcp.listen(port, [:binary, packet: :raw, active: false, reuseaddr: true])

    IO.puts "\n Listening for connection requests on port #{port}...\n"

    accept_loop(listen_socket)
  end

  @doc """
  Accepts client connections on the `listen_socket`.
  """
  def accept_loop(listen_socket) do
    IO.puts "Waiting to accept a client connection...\n"

    # Suspends (blocks) and waits for a client connection. When a connection is accepted, `client_socket` is bound
    # to a new client socket.
    {:ok, client_socket} = :gen_tcp.accept(listen_socket)

    IO.puts "Connection accepted!\n"

    # Receives the request and sends a response over the client socket.
#    serve(client_socket)
    spawn(fn -> serve(client_socket) end)

    # Loop back to wait and accept the next connection.
    accept_loop(listen_socket)
  end

  def serve(client_socket) do
    IO.puts "#{inspect self()}: Working on it!"

    client_socket
    |> read_request
#    |> generate_response
    |> Handler.handle
    |> write_response(client_socket)
  end

  @doc """
  Receives a request on the `client_socket`.
  """
  def read_request(client_socket) do
    {:ok, request} = :gen_tcp.recv(client_socket, 0) # all available bytes

    IO.puts "Received request:\n"
    IO.puts request

    request
  end

  def generate_response(_request) do
    """
    HTTP/1.1 200 OK\r
    Content-Type: text/plain\r
    Content-Length: 6\r
    \r
    Hello!
    """
  end

  @doc """
  Sends the `response` over the `client_socket`.
  """
  def write_response(response, client_socket) do
    :ok = :gen_tcp.send(client_socket, response)

    IO.puts "Send response:\n"
    IO.puts response

    # Closes the client socket, ending the connection. Does not close the listen socket!
    :gen_tcp.close(client_socket)
  end
  
end