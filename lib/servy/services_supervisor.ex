defmodule Servy.ServicesSupervisor do
  @moduledoc false
  
  use Supervisor

  alias Servy.{PledgeServer, SensorServer}


  # client api

  def start_link(_arg) do
    IO.puts "Starting the services supervisor..."

    Supervisor.start_link __MODULE__, :ok, name: __MODULE__
  end

  # callbacks

  def init(:ok) do
    children = [
      PledgeServer,
      {SensorServer, 60},
    ]

    Supervisor.init children, strategy: :one_for_one
  end

end


# example
# $ iex -S mix
# iex> alias Servy.ServicesSupervisor
# iex> {:ok, sup_pid} = ServicesSupervisor.start_link
# iex> Supervisor.which_children sup_pid
# iex> Supervisor.count_children sup_pid
# iex> Process.whereis(:sensor_server) |> Process.exit(:kaboom)
# iex> Process.whereis(:pledge_server) |> Process.exit(:kaboom)
# iex> Servy.PledgeServer.child_spec []
