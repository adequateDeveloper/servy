defmodule Servy.Handler do
  @moduledoc """
  HTTP request / response handler
  """

  alias Servy.{Api, BearController, Conversation, Parser, PledgeController, Plugins, SensorServer}

  require IO
  require Logger
  # import Servy.Plugins, only: [rewrite_path: 1, log_route: 1, track_404_response: 1]

  @module __MODULE__
  @pages_path Path.expand("pages", File.cwd!)
  # @pages_path Path.expand("../../pages", __DIR__)

  @doc """
  process a request into a response
  """
  @spec handle(String.t) :: String.t
  def handle(request) do
    Logger.debug fn -> "#{@module}.handle(request): #{inspect request}" end

    request
      |> Parser.parse
      |> Plugins.rewrite_path
#      |> Plugins.log_route
      |> route
      |> Plugins.track_404_response
      |> format_response
  end

  @doc """
  get video cam snapshots

  iex> spawn(Servy.HttpServer, :start, [4000])
  $ curl http://localhost:4000/snapshots
  $ curl http://localhost:4000/sensors

  iex> Servy.HttpServer.start(4000)
  """
#  def route(%Conversation{method: "GET", path: "/snapshots"} = conversation) do
  def route(%Conversation{method: "GET", path: "/sensors"} = conversation) do

#  v1 -----------------------------------------------------------
#    snapshot1 = VideoCam.get_snapshot "cam-1"
#    snapshot2 = VideoCam.get_snapshot "cam-2"
#    snapshot3 = VideoCam.get_snapshot "cam-3"

#    snapshots = [snapshot1, snapshot2, snapshot3]
# ---------------------------------------------------------------
#  v2 -----------------------------------------------------------
#    caller = self()

#    spawn(fn -> send(caller, {:result, VideoCam.get_snapshot("cam-1")}) end)
#    spawn(fn -> send(caller, {:result, VideoCam.get_snapshot("cam-2")}) end)
#    spawn(fn -> send(caller, {:result, VideoCam.get_snapshot("cam-3")}) end)

#    snapshot1 = receive do {:result, filename} -> filename end
#    snapshot2 = receive do {:result, filename} -> filename end
#    snapshot3 = receive do {:result, filename} -> filename end

#    snapshots = [snapshot1, snapshot2, snapshot3]
#  --------------------------------------------------------------------
#  v3 -----------------------------------------------------------
#    Fetcher.async("cam-1")
#    Fetcher.async("cam-2")
#    Fetcher.async("cam-3")

#    snapshot1 = Fetcher.get_result()
#    snapshot2 = Fetcher.get_result()
#    snapshot3 = Fetcher.get_result()

#    snapshots = [snapshot1, snapshot2, snapshot3]
# --------------------------------------------------------------------------------
#  v4 -----------------------------------------------------------

#    Fetcher.async(fn -> VideoCam.get_snapshot("cam-1") end)
#    Fetcher.async(fn -> VideoCam.get_snapshot("cam-2") end)
#    Fetcher.async(fn -> VideoCam.get_snapshot("cam-3") end)
#    Fetcher.async(fn -> Tracker.get_location("bigfoot") end)

#    snapshot1 = Fetcher.get_result()
#    snapshot2 = Fetcher.get_result()
#    snapshot3 = Fetcher.get_result()
#    where_is_bigfoot = Fetcher.get_result()

#    snapshots = [snapshot1, snapshot2, snapshot3]

#    %{conversation | status: 200, response_body: inspect {snapshots, where_is_bigfoot}}
# --------------------------------------------------------------------------------

#    pid1 = Fetcher.async(fn -> VideoCam.get_snapshot("cam-1") end)
#    pid2 = Fetcher.async(fn -> VideoCam.get_snapshot("cam-2") end)
#    pid3 = Fetcher.async(fn -> VideoCam.get_snapshot("cam-3") end)
#    pid4 = Fetcher.async(fn -> Tracker.get_location("bigfoot") end)

#    where_is_bigfoot = Fetcher.get_result(pid4)
#    snapshot1 = Fetcher.get_result(pid1)
#    snapshot2 = Fetcher.get_result(pid2)
#    snapshot3 = Fetcher.get_result(pid3)

#    snapshots = [snapshot1, snapshot2, snapshot3]
# --------------------------------------------------------------------------------

#  v5 -----------------------------------------------------------

#    pid4 = Fetcher.async(fn -> Tracker.get_location("bigfoot") end)

#    snapshots =
#      ["cam-1", "cam-2", "cam-3"]
#      |> Enum.map(&Fetcher.async(fn -> VideoCam.get_snapshot(&1) end))
#      |> Enum.map(&Fetcher.get_result/1)

#    where_is_bigfoot = Fetcher.get_result(pid4)
# --------------------------------------------------------------------------------

#  v6 -----------------------------------------------------------

#    task = Task.async(fn -> Tracker.get_location("bigfoot") end)
#
#    snapshots =
#      ["cam-1", "cam-2", "cam-3"]
#      |> Enum.map(&Task.async(fn -> VideoCam.get_snapshot(&1) end))
#      |> Enum.map(&Task.await/1)
#
#    where_is_bigfoot = Task.await(task)

    sensor_data = SensorServer.get_sensor_data()

    %{conversation | status: 200, response_body: inspect sensor_data}
  end


  @doc """
  post pledge
  """
  def route(%Conversation{method: "POST", path: "/pledges"} = conversation) do
    PledgeController.create(conversation, conversation.params)
  end

  @doc """
  get pledges
  """
  def route(%Conversation{method: "GET", path: "/pledges"} = conversation) do
    PledgeController.index(conversation)
  end

  @doc """
  deliberately blow up a request
  """
  def route(%Conversation{method: "GET", path: "/kaboom"} = _conversation) do
    raise "Kaboom!"
  end

  @doc """
  pause for `time` then transform a request conversation into a response conversation with a response body
  """
  def route(%Conversation{method: "GET", path: "/hibernate/" <> time} = conversation) do
    time |> String.to_integer |> :timer.sleep
    %{conversation | status: 200, response_body: "Awake!"}
  end

  @doc """
  transform a request conversation into a response conversation with a response body
  """
  @spec route(%Conversation{}) :: %Conversation{}
  def route(%Conversation{method: "GET", path: "/wildthings"} = conversation) do
    %{conversation | status: 200, response_body: "Bears, Lions, Tigers"}
  end

  def route(%Conversation{method: "GET", path: "/bears/new"} = conversation) do
      @pages_path
      |> Path.join("form.html")
      |> File.read
      |> handle_file(conversation)
  end

  def route(%Conversation{method: "GET", path: "/bears"} = conversation) do
    # res = BearController.index conversation
    # Logger.debug fn -> "#{@module}.route({method: GET, path: /bears}, res)" end

    BearController.index conversation
  end

  def route(%Conversation{method: "GET", path: "/api/bears"} = conversation) do
    Api.BearController.index conversation
  end

  def route(%Conversation{method: "GET", path: "/grizzly"} = conversation) do
    BearController.index_grizzly conversation
  end

  def route(%Conversation{method: "GET", path: "/bears/" <> id} = conversation) do
    params = Map.put(conversation.params, "id", id)
    BearController.show(conversation, params)
    # %{conversation | status: 200, response_body: "Bear #{id}"}
  end

  def route(%Conversation{method: "POST", path: "/bears"} = conversation) do
    BearController.create(conversation, conversation.params)
    # type = conversation.params["type"]
    # name = conversation.params["name"]
    # response = "Created a #{type} bear named #{name}!"
    # %{conversation | status: 201, response_body: response}
  end

  def route(%Conversation{method: "DELETE", path: "/bears/" <> _id} = conversation) do
    BearController.delete conversation
  end

  def route(%Conversation{method: "GET", path: "/about"} = conversation) do
      @pages_path
      |> Path.join("about.html")
      |> File.read
      |> handle_file(conversation)
  end

  def route(conversation) do
    %{conversation | status: 404, response_body: "No #{conversation.path} here!"}
  end

  @doc """
  process file content to the response body
  """
  @spec handle_file({Atom.t, any()}, %Conversation{}) :: %Conversation{}
  def handle_file({:ok, content}, %Conversation{} = conversation) do
    Logger.debug fn -> "#{@module}.handle_file({:ok, content}): #{inspect conversation}" end
    %{conversation | status: 200, response_body: content}
  end

  def handle_file({:error, :enoent}, %Conversation{} = conversation) do
    Logger.debug fn -> "#{@module}.handle_file({:error, :enoent}): #{inspect conversation}" end
    %{conversation | status: 404, response_body: "File not found!"}
  end

  def handle_file({:error, reason}, %Conversation{} = conversation) do
    Logger.debug fn -> "#{@module}.handle_file({:error, reason}): #{inspect conversation}" end
    %{conversation | status: 500, response_body: "File error: #{reason}"}
  end

  @doc """
  transform a response map into  a valid HTTP response string
  """
  @spec format_response(%Conversation{}) :: String.t
  def format_response(%Conversation{} = conversation) do
    Logger.debug  fn -> "#{@module}.format_response(conversation): #{inspect conversation}" end

    # HTTP/1.1 #{conversation.status} #{status_reason(conversation.status)}
    # HTTP/1.1 #{conversation.status} #{@status_reason[conversation.status]}

    # The HTTP spec requires each line to end in '\r\n'. Heredoc automatically
    # provides the '\n' so the '\r' must be explicity added.

    """
    HTTP/1.1 #{Conversation.full_status(conversation)}\r
    Content-Type: #{conversation.response_content_type}\r
    Content-Length: #{byte_size(conversation.response_body)}\r
    \r
    #{conversation.response_body}
    """
  end

end

# request = """
# GET /wildthings HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*
#
# """
#
# response = Servy.Handler.handle request
# IO.puts response

# request = """
# GET /bears HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*
#
# """
#
# response = Servy.Handler.handle request
# IO.puts response

# request = """
# GET /bigfoot HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*
#
# """
#
# response = Servy.Handler.handle request
# IO.puts response

# request = """
# GET /bears/1 HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*
#
# """
#
# response = Servy.Handler.handle request
# IO.puts response

# request = """
# GET /wildlife HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*
#
# """
#
# response = Servy.Handler.handle request
# IO.puts response

# request = """
# GET /about HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*
#
# """
#
# response = Servy.Handler.handle request
# IO.puts response

# request = """
# GET /bears/new HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*
#
# """
#
# response = Servy.Handler.handle request
# IO.puts response

# request = """
# POST /bears HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*
# Content-Type: application/x-www-form-urlencoded
# Content-Length: 21
#
# name=Baloo&type=Brown
# """
#
# response = Servy.Handler.handle request
# IO.puts response
