defmodule Servy.Wildthings do
  @moduledoc """
  Controller for wildthings

  Example

      iex> alias Servy.Wildthings
      Servy.Wildthings
      iex> Wildthings.list_bears
      [%Servy.Bear{hibernating: true, id: 1, name: "Teddy", type: "Brown"},
       %Servy.Bear{hibernating: false, id: 2, name: "Smokey", type: "Black"},
       %Servy.Bear{hibernating: false, id: 3, name: "Paddington", type: "Brown"},
       %Servy.Bear{hibernating: true, id: 4, name: "Scarface", type: "Grizzly"},
       %Servy.Bear{hibernating: false, id: 5, name: "Snow", type: "Polar"},
       %Servy.Bear{hibernating: false, id: 6, name: "Brutus", type: "Grizzly"},
       %Servy.Bear{hibernating: true, id: 7, name: "Rosie", type: "Black"},
       %Servy.Bear{hibernating: false, id: 8, name: "Roscoe", type: "Panda"},
       %Servy.Bear{hibernating: true, id: 9, name: "Iceman", type: "Polar"},
       %Servy.Bear{hibernating: false, id: 10, name: "Kenai", type: "Grizzly"}]

  """

  alias Servy.Bear

  @doc """
  List of Bear examples
  """
  def list_bears do
    [
      %Bear{id: 1, name: "Teddy", type: "Brown", hibernating: true},
      %Bear{id: 2, name: "Smokey", type: "Black"},
      %Bear{id: 3, name: "Paddington", type: "Brown"},
      %Bear{id: 4, name: "Scarface", type: "Grizzly", hibernating: true},
      %Bear{id: 5, name: "Snow", type: "Polar"},
      %Bear{id: 6, name: "Brutus", type: "Grizzly"},
      %Bear{id: 7, name: "Rosie", type: "Black", hibernating: true},
      %Bear{id: 8, name: "Roscoe", type: "Panda"},
      %Bear{id: 9, name: "Iceman", type: "Polar", hibernating: true},
      %Bear{id: 10, name: "Kenai", type: "Grizzly"}
    ]
  end

  @doc """
  Get a bear by its numeric id
  """
  def get_bear(id) when is_integer(id) do
    Enum.find(list_bears(), fn(bear) -> bear.id == id end)
  end

  @doc """
  Get a bear by its binary id
  """
  def get_bear(id) do
    id
      |> String.to_integer
      |> get_bear
  end

end
