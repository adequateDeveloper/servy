defmodule Servy.Parser do
  @moduledoc """
  Servy HTTP request parser
  """

  alias Servy.Conversation
  require Logger

  @module __MODULE__

  @doc """
  parse a request string into a %Conversation{}
  """
  @spec parse(String.t) :: %Conversation{}
  def parse(request) do
    Logger.debug fn -> "#{@module}.parse(request): #{inspect request}" end

    # [method, path, _http_version] =
    #   request
    #     |> String.split("\n")
    #     |> List.first
    #     |> String.split(" ")

    # The HTTP spec requires each line to end in '\r\n'.

    [headers, body] = String.split(request, "\r\n\r\n")
    [request_line | header_lines] = String.split(headers, "\r\n")
    [method, path, _http_version] = String.split(request_line, " ")

    headers = parse_headers(header_lines, %{})
    params = parse_body(headers["Content-Type"], body)

    %Conversation{method: method, headers: headers, params: params, path: path, status: 0}
  end

  @spec parse_headers([...], Map.t) :: Map.t
  def parse_headers([head | tail], headers) do
    [key, value] = String.split(head, ": ")
    headers = Map.put(headers, key, value)
    parse_headers(tail, headers)
  end

  def parse_headers([], headers), do: headers

  @spec parse_body(String.t, [...]) :: Map.t
  def parse_body("application/x-www-form-urlencoded", body) do
    body
    |> String.trim
    |> URI.decode_query
  end

  def parse_body(_content_type, _body), do: %{}

end
