defmodule Servy.SensorServer do
  @moduledoc false

  @name :sensor_server
  @refresh_interval :timer.seconds(60)

  use GenServer

  alias Servy.{Tracker, VideoCam}


  # client interface

  def start, do: GenServer.start __MODULE__, %{}, name: @name

  def start_link(interval) do
    IO.puts "Starting the sensor server with #{interval} min refresh..."
    GenServer.start_link __MODULE__, %{}, name: @name   # for ServicesSupervisor.start_link
  end

  def get_sensor_data, do: GenServer.call @name, :get_sensor_data


  # server callbacks

  def init(_state) do
    initial_state = run_tasks_to_get_sensor_data()
    schedule_refresh_cache()
    {:ok, initial_state}
  end

  def handle_call(:get_sensor_data, _from, state), do: {:reply, state, state}

  def handle_info(:refresh_cache, _state) do
    IO.puts "Refreshing the cache..."
    new_state = run_tasks_to_get_sensor_data()
    schedule_refresh_cache()
    {:noreply, new_state}
  end


  # callbacks support

  defp run_tasks_to_get_sensor_data do
    IO.puts "Running tasks to get sensor data..."

    task = Task.async(fn -> Tracker.get_location("bigfoot") end)

    snapshots =
      ["cam-1", "cam-2", "cam-3"]
      |> Enum.map(&Task.async(fn -> VideoCam.get_snapshot(&1) end))
      |> Enum.map(&Task.await/1)

    where_is_bigfoot = Task.await(task)

    %{snapshots: snapshots, location: where_is_bigfoot}
  end

  defp schedule_refresh_cache do
    Process.send_after self(), :refresh_cache, @refresh_interval
  end

end


# example commands

#alias Servy.SensorServer
#
#{:ok, _pid} = SensorServer.start
#
#IO.inspect SensorServer.get_sensor_data

