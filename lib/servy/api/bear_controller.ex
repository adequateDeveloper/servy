defmodule Servy.Api.BearController do
  @moduledoc """
  Controller for bears json requests
  """

  alias Servy.Wildthings

  require IO
  require Logger

  @module __MODULE__

  @doc """
  GET /bears in json
  """
  def index(conversation) do
    bears =
      Wildthings.list_bears
#      |> Enum.sort(&Bear.order_asc_by_name/2)
      |> Poison.encode!

    Logger.debug fn -> "#{@module}.index(conversation): #{inspect bears}" end

    %{conversation | status: 200, response_content_type: "application/json", response_body: bears}
  end

end
