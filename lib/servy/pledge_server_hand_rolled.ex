defmodule Servy.GenericServer do

  def start(callback_module, initial_state, name) do
    pid = spawn(__MODULE__, :listen_loop, [initial_state, callback_module])
    Process.register pid, name
    pid
  end

  def call(server, message) do
    # async
    send server, {:call, self(), message}

    # blocking
    receive do {:response, response} -> response end
  end

  def cast(server, message), do: send server, {:cast, message}

  def listen_loop(state, callback_module) do
    receive do
      {:call, sender, message} when is_pid sender ->
        {response, new_state} = callback_module.handle_call message, state
        send sender, {:response, response}
        listen_loop new_state, callback_module

      {:cast, message} ->
        new_state = callback_module.handle_cast(message, state)
        listen_loop new_state, callback_module

      unknown ->
        IO.puts "Unknown message #{inspect unknown}"
        listen_loop state, callback_module
    end
  end

end


defmodule Servy.PledgeServerHandRolled do
  @moduledoc false

  # iex> pid = spawn(Servy.PledgeServerHandRolled, :listen_loop, [[]])
  # iex> send pid, {:create_pledge, "larry", 10}
  # iex> send pid, {:create_pledge, "moe", 20}
  # iex> send pid, {:create_pledge, "curly", 30}
  # iex> send pid, {self(), :recent_pledges}
  # iex> Process.info(self(), :messages)
  # iex> receive do {:response, recent_pledges} -> recent_pledges end
  # iex> send pid, {self(), :recent_pledges}
  # iex> :observer.start
  # go to iex pid and look in the messages tab

  @name :pledge_server_hand_rolled

  alias Servy.GenericServer


  # Client Pledge Interfaces

  def start, do: GenericServer.start __MODULE__, [], @name

  def create_pledge(name, amount), do: GenericServer.call @name, {:create_pledge, name, amount}

  def recent_pledges, do: GenericServer.call @name, :recent_pledges

  def total_pledged, do: GenericServer.call @name, :total_pledged

  def clear, do: GenericServer.cast @name, :clear


  # Server Callbacks

  def handle_call(:total_pledged, state) do
    # build a list of just the amounts from the list of tuples, then sum the amount list
    total = Enum.map(state, &elem(&1, 1)) |> Enum.sum       # 0 = name, 1 = amount
    {total, state}
  end

  def handle_call(:recent_pledges, state), do: {state, state}

  def handle_call({:create_pledge, name, amount}, state) do
    {:ok, id} = send_pledge_to_service(name, amount)
    top_two_pledges = Enum.take(state, 2)
    new_state = [{name, amount} | top_two_pledges]
    {id, new_state}
  end
  
  def handle_cast(:clear, _state), do: []


  # Callback Helpers

  defp send_pledge_to_service(_name, _amount) do
    {:ok, "pledge-#{:rand.uniform(1000)}"}
  end
  
end


# test code - runs in the client process
#alias Servy.PledgeServerHandRolled
#
#IO.inspect self()
#IO.puts "...is spawning the PledgeServerHandRolled..."
#server_pid = PledgeServerHandRolled.start
#IO.inspect server_pid
#
#IO.puts "\nCreating a series of pledges..."
#IO.inspect PledgeServerHandRolled.create_pledge("larry", 10)
#IO.inspect PledgeServerHandRolled.create_pledge("moe", 20)
#IO.inspect PledgeServerHandRolled.create_pledge("curly", 30)
#IO.inspect PledgeServerHandRolled.create_pledge("daisy", 40)
#
#PledgeServerHandRolled.clear
#
#IO.inspect PledgeServerHandRolled.create_pledge("grace", 50)
#
#IO.puts "\nRequesting the most recent pledges..."
#IO.inspect PledgeServerHandRolled.recent_pledges
#
#IO.inspect PledgeServerHandRolled.total_pledged
#
#send server_pid, {:unknown, "unknown_message"}

