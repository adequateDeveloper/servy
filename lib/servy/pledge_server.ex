defmodule Servy.PledgeServer do
  @moduledoc false

  use GenServer
#  use GenServer, restart: :temporary

  @name :pledge_server


  defmodule State do
    defstruct cache_size: 3, pledges: []
  end

  # Client Pledge Interface

  def start, do: GenServer.start __MODULE__, %State{}, name: @name

  def start_link(_arg) do
    IO.puts "Starting the pledge server..."
    GenServer.start_link __MODULE__, %State{}, name: @name    # for ServicesSupervisor.start_link
  end

#  def child_spec(arg) do
#    %{id: Servy.PledgeServer, restart: :temporary, shutdown: 5000,
#      start: {Servy.PledgeServer, :start_link, [[]]}, type: :worker}
#  end

  def create_pledge(name, amount), do: GenServer.call @name, {:create_pledge, name, amount}

  def recent_pledges, do: GenServer.call @name, :recent_pledges

  def total_pledged, do: GenServer.call @name, :total_pledged

  def clear, do: GenServer.cast @name, :clear

  def set_cache_size(size), do: GenServer.cast @name, {:set_cache_size, size}


  # Server Callbacks

  def init(state) do
    pledges = fetch_recent_pledges_from_service()
    new_state = %{state | pledges: pledges}
    {:ok, new_state}
  end

  def handle_call(:recent_pledges, _from, state), do: {:reply, state.pledges, state}

  def handle_call({:create_pledge, name, amount}, _from, state) do
    {:ok, id} = send_pledge_to_service(name, amount)
    top_two_pledges = Enum.take(state.pledges, state.cache_size - 1)
    cached_pledges = [{name, amount} | top_two_pledges]
    new_state = %{state | pledges: cached_pledges}
    {:reply, id, new_state}
  end

  def handle_call(:total_pledged, _from, state) do
    # build a list of just the amounts from the list of tuples, then sum the amount list
    total = Enum.map(state.pledges, &elem(&1, 1)) |> Enum.sum       # 0 = name, 1 = amount
    {:reply, total, state}
  end

  def handle_cast(:clear, state), do: {:noreply, %{state | pledges: []}}

  def handle_cast({:set_cache_size, size}, state) do
    new_state = %{state | cache_size: size}
    {:noreply, new_state}
  end

  def handle_info(message, state) do
    IO.puts "Unknown message #{inspect message}"
    {:noreply, state}
  end


  # Server Callback Helpers

  defp send_pledge_to_service(_name, _amount) do
    {:ok, "pledge-#{:rand.uniform(1000)}"}
  end

  def fetch_recent_pledges_from_service, do: [{"wilma", 15}, {"fred", 25}]

end


# test code - runs in the client process
#alias Servy.PledgeServer
#
#IO.inspect self()
#IO.puts "...is spawning the PledgeServer..."
#{:ok, server_pid} = PledgeServer.start
#IO.inspect server_pid
#
#IO.puts "\nCreating a series of pledges..."
#IO.inspect PledgeServer.create_pledge("larry", 10)
#
#PledgeServer.set_cache_size 4
#
#PledgeServer.clear
#
#IO.inspect PledgeServer.create_pledge("moe", 20)
#IO.inspect PledgeServer.create_pledge("curly", 30)
#IO.inspect PledgeServer.create_pledge("daisy", 40)
#IO.inspect PledgeServer.create_pledge("grace", 50)
#
#IO.puts "\nRequesting the most recent pledges..."
#IO.inspect PledgeServer.recent_pledges
#
#IO.inspect PledgeServer.total_pledged
#
#send server_pid, {:unknown, "unknown_message"}

