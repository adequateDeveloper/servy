defmodule Servy.Plugins do
  @moduledoc """
  Utility functions for handler module
  """

  require Logger
  alias Servy.Conversation

  @module __MODULE__

  @doc """
  rewrite path '/wildlife' to '/wildthing'
  """
  @spec rewrite_path(%Conversation{}) :: %Conversation{}
  def rewrite_path(%Conversation{path: "/wildlife"} = conversation) do
    Logger.debug fn -> "#{@module}.rewrite_path(conversation): #{inspect conversation}" end
    %{conversation | path: "/wildthings"}
  end

  def rewrite_path(%Conversation{} = conversation), do: conversation

  @doc """
  log the route request_response structure
  """
  @spec log_route(%Conversation{}) :: %Conversation{}
  def log_route(%Conversation{} = conversation) do
    Logger.debug fn -> "#{@module}.log_route(conversation): #{inspect conversation}" end
    conversation
  end

  @doc """
  track and log 404 responses
  """
  @spec track_404_response(%Conversation{}) :: %Conversation{}
  def track_404_response(%Conversation{status: 404} = conversation) do
    # if Mix.env != :test do
    #   IO.puts "Warning: 404 response"
    # end

    Logger.debug  fn -> "#{@module}.track_404_response(conversation): #{inspect conversation}" end
    conversation
  end

  def track_404_response(%Conversation{} = conversation), do: conversation

  # def log(%Conversation{} = conversation) do
  #   if Mix.env == :dev do
  #     IO.inspect conversation
  #   end
  #   conversation
  # end

end
