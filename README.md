# Servy

**Following the Pragmatic Studio course 'Developing With Elixir' at: https://pragmaticstudio.com/courses/elixir**

## Features

* Module, Type & Function Docs
* Logging
* Unit Tests
  * CaptureLog Test
* Credo package for static code analysis
* Poison package for JSON handling
* Example of Erlang-to-Elixir code conversion (see http_server.ex) 
* Dialyzir package for static code analysis
* ExDoc package for API doc generation
* CI build status

[![Build Status](https://semaphoreci.com/api/v1/adequatedeveloper/servy/branches/master/badge.svg)](https://semaphoreci.com/adequatedeveloper/servy)
