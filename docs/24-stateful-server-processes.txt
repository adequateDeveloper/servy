Stateful Server Processes
Notes

Prepared Code
Here's the PledgeController module we had defined in the pledge_controller.ex file at the start of the video:

  defmodule Servy.PledgeController do
    def create(conv, %{"name" => name, "amount" => amount}) do
      # Sends the pledge to the external service and caches it
      create_pledge(name, String.to_integer(amount))

      %{ conv | status: 201, resp_body: "#{name} pledged #{amount}!" }
    end

    def index(conv) do
      # Gets the recent pledges from the cache
      pledges = recent_pledges()

      %{ conv | status: 200, resp_body: (inspect pledges) }
    end
  end

Here are the two routes we added to handler.ex:

  def route(%Conv{method: "POST", path: "/pledges"} = conv) do
    Servy.PledgeController.create(conv, conv.params)
  end

  def route(%Conv{method: "GET", path: "/pledges"} = conv) do
    Servy.PledgeController.index(conv)
  end

They handle GET and POST requests for /pledges which are delegated to the index and create functions (actions) of the
PledgeController, respectively.

And here's the code we added to the bottom of pledge_server.ex (outside of the module) to mimic what we did in the iex session:

  alias Servy.PledgeServer

  pid = spawn(PledgeServer, :listen_loop, [[]])

  send pid, {:create_pledge, "larry", 10}
  send pid, {:create_pledge, "moe", 20}
  send pid, {:create_pledge, "curly", 30}
  send pid, {:create_pledge, "daisy", 40}
  send pid, {:create_pledge, "grace", 50}

  send pid, {self(), :recent_pledges}

  receive do {:response, pledges} -> IO.inspect pledges end


Tip: Registering Unique Names

In the video we used a module attribute to store the registered name of the PID as a constant:

  @name :pledge_server

And we registered the PID under that name:

  Process.register(pid, @name)

Then, to send messages to the server process, we used the registered name rather than the server's PID. For example:

  send @name, {self(), :create_pledge, name, amount}

  send @name, {self(), :recent_pledges}

  send @name, {self(), :total_pledged}

As we mentioned, an error is raised if you try to register a process under the same name as another process.
Instead of trying to come up with a unique name under which to register the PID, another option is to register the PID
under the name __MODULE__, like so:

  @name __MODULE__

At compile time __MODULE__ is expanded to the name of the current module, which means the PID will be registered under
the name Servy.PledgeServer in this case. And because module names must always be unique in a project, using this
technique ensures that the PID will always be registered under a unique name.


Finding a Registered Process

We defined the PledgeServer.start function to register the spawned process under the name :pledge_server:

  iex> Servy.PledgeServer.start()
  #PID<0.158.0>

Suppose you wanted to get the PID registered under that name. That's when the Process.whereis function comes in handy:

  iex> Process.whereis(:pledge_server)
  #PID<0.158.0>

Notice it returns the same PID that was returned by the start function.

The Process module includes other functions for managing registered processes. For example, you can remove any
registrations associated with a name using the Process.unregister function:

  iex> Process.unregister(:pledge_server)
  true

Now if we call Process.whereis to find the PID associated with the name :pledge_server, the function returns nil because
no process is registered under that name:

  iex> Process.whereis(:pledge_server)
  nil

Suppose we decide to register our server process under a different name:

  iex> Process.register(pid, :pledgy)
  true

Now the PID can be found under that name:

  iex> Process.whereis(:pledgy)
  #PID<0.158.0>

Finally, you can use the Process.registered function to get a list of all registered processes in the Erlang VM:

  iex> Process.registered()
  [IEx.Supervisor, :httpc_hex, :standard_error_sup, :ssl_pem_cache, ...]


Tip: Starting a Process With Different Initial States

In the video we only needed to start the PledgeServer process with an empty cache, so we passed an empty list
(the initial state) as the third argument to the spawn function:

  def start do
    IO.puts "Starting the pledge server..."
    pid = spawn(__MODULE__, :listen_loop, [[]])
    Process.register(pid, @name)
    pid
  end

However, you might have a scenario where you want to start a server process with varying initial states, depending on
the situation at hand. To do that, you could change the start function to take an initial_state argument with a default
value and then forward the value of that argument to the spawn function, like so:

  def start(initial_state \\ []) do
    IO.puts "Starting the pledge server..."
    pid = spawn(__MODULE__, :listen_loop, [initial_state])
    Process.register(pid, @name)
    pid
  end

Now when you call start with no argument the initial state will be an empty list:

  iex> Servy.PledgeServer.start()

  iex> Servy.PledgeServer.recent_pledges()
  []

Alternatively, you can call start with a list to pre-populate the cache, like so:

  iex> Servy.PledgeServer.start([{"larry", 10}])

  iex> Servy.PledgeServer.recent_pledges()
  [{"larry", 10}]

This is admittedly a contrived example that doesn't necessarily fit for this server process. But you can imagine
situations where you would want to vary the initial state of another stateful server process depending on certain conditions.


Agents

Sometimes you just want a quick and easy way to store state in a process without any hoopla. Anticipating those
situations, Elixir provides the Agent module. You can think of it as a simple wrapper around a server process that
stores state and offers access to that state through a thin client interface.

It's best just to see an agent in action, so fire up an iex session and we'll take one for a spin...

To start an agent, you call the Agent.start function and pass it a function that returns the initial state. For example,
suppose we want the agent to store a list of pledges, so we'll initialize the agent with an empty list:

  iex> {:ok, agent} = Agent.start(fn -> [] end)
  {:ok, #PID<0.90.0>}

That spawns a process that's holding onto an Elixir list in its memory. Notice the agent variable is bound to a PID.

To add a pledge to the agent's state, you call the Agent.update function with a PID and a function that updates the state:

  iex> Agent.update(agent, fn(state) -> [ {"larry", 10} | state ] end)
  :ok

Notice the function is passed the current state which the function then transforms into the new state. Let's add another
pledge to the head of the list just to make things interesting:

  iex> Agent.update(agent, fn(state) -> [ {"moe", 20} | state ] end)
  :ok

Then to retrieve the current state, you call the Agent.get function with a PID and a function that returns the state.

  iex> Agent.get(agent, fn(state) -> state end)
  [{"moe", 20}, {"larry", 10}]

Again, the function is passed the current state and can return all or part of the state as neccessary.

What could be simpler, right? We didn't have to define a module, write a receive block, maintain state in a recursive
loop, or write any client interface functions. Agent encapsulates all those low-level details and gives us a tidy client
interface.

So why didn't we use Agent rather than hand-rolling our own stateful server process? Well, Agent is great if you simply
need a process to store state. However, that's all an agent can do! It can't also run asynchronous queries or
computations in the server process, such as sending requests to an external service or totaling the pledge amounts.

More often than not you need a server to store state and do other things related to that state. For that reason, you
won't see Agent used very often in the wild. But not to worry: Elixir provides another abstraction called GenServer for
building stateful server processes. That's the topic of the next several sections!


Exercise: Write a Test for Pledge Server
If you want more practice writing tests, write a test for the PledgeServer module. It should assert that the server
caches only the 3 most recent pledges and totals their amounts.


  defmodule PledgeServerTest do
    use ExUnit.Case

    alias Servy.PledgeServer

    test "caches the 3 most recent pledges and totals their amounts" do
      PledgeServer.start()

      PledgeServer.create_pledge("larry", 10)
      PledgeServer.create_pledge("moe", 20)
      PledgeServer.create_pledge("curly", 30)
      PledgeServer.create_pledge("daisy", 40)
      PledgeServer.create_pledge("grace", 50)

      most_recent_pledges = [{"grace", 50}, {"daisy", 40}, {"curly", 30}]

      assert PledgeServer.recent_pledges() == most_recent_pledges

      assert PledgeServer.total_pledged() == 120
    end
  end


Exercise: Create Another Stateful Server Process

Currently, every time our web server gets a request for a path that's not found (a 404) it prints the missing path to
the console. Here's the function currently defined in the Plugins module that does that:

  def track(%Conv{status: 404, path: path} = conv) do
    if Mix.env != :test do
      IO.puts "Warning: #{path} is on the loose!"
    end
    conv
  end

But suppose you want to keep a count of the requests that result in a 404. For example, if you're getting a lot of
requests for /nessie then perhaps there's an opportunity to create a page that lets folks buy Loch Ness Monster
(aka Nessie) t-shirts. After all, if there's demand for Nessie then you should meet it, right?

Create a stateful server process that keeps track of the number of times a particular path has resulted in a 404.
To set up a goal and demonstrate the client interface, copy and paste the following test into a
four_oh_four_counter_test.exs file in the test directory:

  defmodule FourOhFourCounterTest do
    use ExUnit.Case

    alias Servy.FourOhFourCounter, as: Counter

    test "reports counts of missing path requests" do
      Counter.start()

      Counter.bump_count("/bigfoot")
      Counter.bump_count("/nessie")
      Counter.bump_count("/nessie")
      Counter.bump_count("/bigfoot")
      Counter.bump_count("/nessie")

      assert Counter.get_count("/nessie") == 3
      assert Counter.get_count("/bigfoot") == 2

      assert Counter.get_counts == %{"/bigfoot" => 2, "/nessie" => 3}
    end
  end

Try to implement the FourOhFourCounter module from memory before looking at the answer (or the PledgeServer) for
guidance. When the test passes you know you have everything working as expected!

Hint:
The server's state will be an Elixir map where each key is a path and the corresponding value is the number of times
that path has been requested. To update the map, you can use the Map.update function.

  defmodule Servy.FourOhFourCounter do

    @name :four_oh_four_counter

    # Client Interface

    def start do
      IO.puts "Starting the 404 counter..."
      pid = spawn(__MODULE__, :listen_loop, [%{}])
      Process.register(pid, @name)
      pid
    end

    def bump_count(path) do
      send @name, {self(), :bump_count, path}

      receive do {:response, count} -> count end
    end

    def get_counts do
      send @name, {self(), :get_counts}

      receive do {:response, counts} -> counts end
    end

    def get_count(path) do
      send @name, {self(), :get_count, path}

      receive do {:response, count} -> count end
    end

    # Server

    def listen_loop(state) do
      receive do
        {sender, :bump_count, path} ->
          new_state = Map.update(state, path, 1, &(&1 + 1))
          send sender, {:response, :ok}
          listen_loop(new_state)
        {sender, :get_counts} ->
          send sender, {:response, state}
          listen_loop(state)
        {sender, :get_count, path} ->
          count = Map.get(state, path, 0)
          send sender, {:response, count}
          listen_loop(state)
        unexpected ->
          IO.puts "Unexpected message: #{inspect unexpected}"
          listen_loop(state)
      end
    end
  end


OK, now that you have the FourOhFourCounter working in isolation it's time to integrate it into the web server.
Remember that function in the Plugins module that tracks 404s? Change it to include bumping the count of the missing path.

  def track(%Conv{status: 404, path: path} = conv) do
    if Mix.env != :test do
      IO.puts "Warning: #{path} is on the loose!"
      Servy.FourOhFourCounter.bump_count(path)
    end
    conv
  end

Then in handler.ex add a new route for /404s that gets the 404 counts and returns them in the response body. You can get
fancy by creating a template file if you want, but start by simply calling inspect on the counts and putting the result
in the response body.

  def route(%Conv{method: "GET", path: "/404s"} = conv) do
    counts = Servy.FourOhFourCounter.get_counts()

    %{ conv | status: 200, resp_body: inspect counts }
  end

Then fire up all three servers in iex.

  iex> Servy.FourOhFourCounter.start()

  iex> Servy.PledgeServer.start()

  iex> Servy.HttpServer.start(4000)

Then browse to http://localhost:4000/nessie and hit reload a few times to simulate a demand for some stylish aquatic
t-shirts. To see all the sales you're missing out on, browse to http://localhost:4000/404s.


Exercise: POSTing Data to an API

In the video we focused our attention on writing a stateful server process, and therefore we didn't bother to implement
the actual POSTing of a pledge to an external service. Added to which, if you worked through an earlier exercise on
sending a request to an API using HTTPoison, then you already have a start on knowing how to POST data to an API!

As a simple example, suppose we want to POST some JSON representing a pledge to the https://httparrot.herokuapp.com/post
endpoint of the free, no-account-necessary http://httparrot.herokuapp.com/ service. It exists specifically for
experimental testing like we're about to do.

To send an HTTP POST request, call the HTTPoison.post function with an endpoint URL and the body of the request:

  iex> url = "https://httparrot.herokuapp.com/post"

  iex> body = ~s({"name": "larry", "amount": 10})

  iex> {:ok, response} = HTTPoison.post url, body

We used the ~s sigil to generate the double-quoted body string so we didn't have to escape the double quotes contained
in the string.

The post function returns {:ok, response} if the request is successful, where response is an HTTPoison.Response struct.
Otherwise, {:error, reason} is returned. We assume the request succeeds, but you'd want to handle both cases.

You can optionally pass a third argument to specify a list of request headers. For example, when POSTing JSON to an API
you likely want to set the Content-Type header to application/json:

  iex> headers = [{"Content-Type", "application/json"}]

  iex> {:ok, response} = HTTPoison.post url, body, headers

With the HTTPoison.Response struct now bound to the response variable, you can access various fields of the response
such as the status code:

  iex> response.status_code
  200

And just as we did in the earlier exercise, we can parse the response body (a JSON string) into an Elixir map:

  iex> Poison.Parser.parse!(response.body)
  %{"args" => %{}, "data" => "{"name": "larry", "amount": 10}",
    "form" => %{},
    "headers" => %{...},
    "json" => %{"amount" => 10, "name" => "larry"},
    "url" => "http://httparrot.herokuapp.com/post"}

That's all there is to it!


Exercise: Observing Messages

We've been peeking in mailboxes using the Process.info function, but it's handy to know that you can also use the
Observer GUI to see what's in the mailbox of any process.

To set things up, first comment out the clause in the PledgeServer.listen_loop function that handles unexpected messages:

  # unexpected ->
  #  IO.puts "Unexpected messaged: #{inspect unexpected}"
  #  listen_loop(state)

Then hop into an iex session and send 1000 unexpected messages to the server, like so:

  iex> pid = Servy.PledgeServer.start()

  iex> 1..500 |> Enum.each(fn(n) -> send pid, {:stop, "hammertime #{n}"} end)

  iex> for n <- 501..1000, do: send pid, {:stop, "hammertime #{n}"}

We sent the messages two different ways—the first 500 using Enum.each and the last 500 using a comprehension—just to
show you two different ways to do it. :-)

Now the mailbox of the PledgeServer process should contain 1000 messages:

  iex> Process.info(pid, :message_queue_len)
  {:message_queue_len, 1000}

Don't worry: Once the process dies or the Erlang VM exits, the mailbox will be cleaned up.

Now fire up the Observer GUI:

  iex> :observer.start

Go to the Processes tab and sort the table by clicking on the MsgQ column header. This column indicates how many
messages are currently waiting in the mailbox of that process. We sent 1000 messages to the PledgeServer process so it
should be the first process in the table. Double-click that process (anywhere on the line) and a new pane will pop open.
Go to the Messages tab of that new pane and you'll see a list of all the messages that are in the mailbox!

When you're done looking around, don't forget to uncomment the clause that handles unexpected messages!


Exercise: Render the Pledges

In the video we used the curl command-line utility to create pledges and get a list of the most recent pledges, like so:

 curl -XPOST http://localhost:4000/pledges -d 'name=larry&amount=100'
 curl -XPOST http://localhost:4000/pledges -d 'name=moe&amount=200'
 curl -XPOST http://localhost:4000/pledges -d 'name=curly&amount=300'
 curl -XPOST http://localhost:4000/pledges -d 'name=daisy&amount=400'
 curl -XPOST http://localhost:4000/pledges -d 'name=grace&amount=500'

  curl http://localhost:4000/pledges

We didn't bother to create an HTML form to POST pledges from a browser or render the most recent pledges on an HTML
page... because you already know how to do that!

Give it a try on your own first before following the steps below. You'll need to create two templates files: one that
renders an HTML form that POSTs the pledge and the other that renders a list of recent pledges.

In the templates directory, create a template named new_pledge.eex that renders the HTML form. The form needs to capture
the name and amount, then POST them to /pledges when the submit button is clicked.

  <h1>Create A Pledge</h1>

  <form action="/pledges" method="POST">
    <p>
      Name:<br/>
      <input type="text" name="name">
    </p>
    <p>
      Amount:<br/>
      <input type="text" name="amount">
    </p>
    <p>
      <input type="submit" value="Create Pledge">
    </p>
  </form>

In the same directory, create a recent_pledges.eex template that renders the pledges assumed to be in the pledges
variable. Remember, the controller already assigns the recent pledges to a pledges variable. It's a list of tuples where
the first element is the name and the second element in the amount.

  <h1>Recent Pledges</h1>

  <ul>
    <%= for pledge <- pledges do %>
      <li><%= elem(pledge, 0) %> pledged <%= elem(pledge, 1) %></li>
    <% end %>
  </ul>

  <a href="/pledges/new">Make a pledge!</a>

With the two templates complete, it's time to move on to the controller. In the PledgeController module, start by
importing the View module which defines the render convenience function.

  import Servy.View

In the index action, call the View.render function to render the recent_pledges.eex template with the pledges variable
as a binding.

  def index(conv) do
    pledges = Servy.PledgeServer.recent_pledges()

    render(conv, "recent_pledges.eex", pledges: pledges)
  end

That should take care of displaying recent pledges.

Next up you need to arrange things to display the pledge form. To do that, define a new action in the PledgeController
that simply renders the new_pledge.eex template.

  def new(conv) do
    render(conv, "new_pledge.eex")
  end

Finally, add a new route to handler.ex that delegates GET requests for /pledges/new to the new action you just defined
in the PledgeController.

  def route(%Conv{method: "GET", path: "/pledges/new"} = conv) do
    Servy.PledgeController.new(conv)
  end

Now, after firing up the PledgeServer and HttpServer, you should be able to browse to http://localhost:4000/pledges/new
and see a pledge form. Use this form to create a few pledges. Then browse to http://localhost:4000/pledges and you
should see the most recent pledges!

Code So Far
The code for this video is in the 'stateful-server' directory found within the video-code directory of the code bundle.

















