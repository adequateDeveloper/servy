Fault Recovery with OTP Supervisors

Notes

Elixir 1.5 Required
The supervisor code we showed in the video will only work with Elixir 1.5 or higher. So in the off chance you're still
running Elixir 1.4, you'll need to install Elixir 1.5 or higher.

In terms of changes to your project files, the generated mix.exs file changed very slightly in Elixir 1.5. Here's the
updated file which you can copy/paste into your mix.exs file:

  defmodule Servy.Mixfile do
    use Mix.Project

    def project do
      [
        app: :servy,
        version: "0.1.0",
        elixir: "~> 1.5",
        start_permanent: Mix.env == :prod,
        deps: deps()
      ]
    end

    # Run "mix help compile.app" to learn about applications.
    def application do
      [
        extra_applications: [:logger]
      ]
    end

    # Run "mix help deps" to learn about dependencies.
    defp deps do
      [{:poison, "~> 3.1"}]
    end
  end


Defining Supervisors Prior to Elixir 1.5

Elixir 1.5 (which we used in the video) streamlines how supervisors are defined as compared to Elixir 1.4.
Using Elixir 1.4, we would have defined the ServicesSupervisor.init function like so:

  def init(:ok) do
    children = [
      worker(Servy.PledgeServer, [[]]),
      worker(Servy.SensorServer, [60])
    ]

    supervise(children, strategy: :one_for_one)
  end

The worker and supervise functions are imported into any module that uses the Supervisor behavior.
The worker function defines the specified module as a worker which will be started with the given list of arguments.
Then the supervise function takes a list of children to supervise and a keyword list of options.

And here's how we would have defined the top-level Supervisor.init function using Elixir 1.4:

  def init(:ok) do
    children = [
      worker(Servy.KickStarter, [4000]),
      supervisor(Servy.ServicesSupervisor, [[]])
    ]

    supervise(children, strategy: :one_for_one)
  end

The supervisor function is also imported into any module that uses the Supervisor behavior. The supervisor function
specifies the given module as a supervisor which will be started with the given list of arguments. In this case, the
list of children is comprised of a worker and a supervisor.

The worker, supervisor, and supervise functions are deprecated as of Elixir 1.5.

Customization Options

As we mentioned in the video, you have a lot of flexibility when it comes to overriding the default child specification
for custom behavior. Let's walk through a few of the customization techniques...


Using Module Names

OK, so this first technique doesn't customize anything. But it's good to know the baseline behavior before moving on to
more advanced scenarios.

The simplest way to list children is using their modules names, like so:

  children = [
    Servy.PledgeServer,
    Servy.SensorServer
  ]

The child_spec function will get invoked on each of the given modules, and an empty list will be passed as the argument.
In other words, the invocation will be child_spec([]).

Here's the default implementation of the child_spec function that's injected when a module has the use GenServer line:

  def child_spec(arg) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [arg]},
      restart: :permanent,
      shutdown: 5000,
      type: :worker
    }
  end

Notice that the argument (arg) is passed through as the argument to the start_link function. So when child_spec([]) is
invoked, the arg is replaced with [] and the function returns the following child specification:

  %{
    id: __MODULE__,
    start: {__MODULE__, :start_link, [[]]},
    restart: :permanent,
    shutdown: 5000,
    type: :worker
  }

Since an empty list was passed as the argument to child_spec, that same empty list is substituted into the map that
represents the child specification. The empty list will be passed to the start_link function. Therefore, the start_link
function must be defined to accept an argument. For example, here's how we defined the PledgeServer.start_link function:

  def start_link(_arg) do
    IO.puts "Starting the pledge server..."
    GenServer.start_link(__MODULE__, %State{}, name: @name)
  end

We didn't have a need for the argument (an empty list) so we ignored it.


Configuring a Child

If you need to configure a child when it's started, you can pass it an argument other than an empty list. To do that,
you specify a child using a {module, arg} tuple, like so:

  children = [
    Servy.PledgeServer,
    {Servy.SensorServer, 60}
  ]

The first element of the tuple is the module containing the child implementation. The second element of the tuple, the
arg, will be passed as the argument to the child's start_link function.

In this case the argument is the number 60 which indicates the refresh interval. Here's how we defined the
SensorServer.start_link function to accept the interval:

  def start_link(interval) do
    IO.puts "Starting the sensor server with #{interval} min refresh..."
    GenServer.start_link(__MODULE__, %{}, name: @name)
  end

The argument can be whatever is necessary to configure the child. For example, you might choose to pass a list of
keyword options:

  children = [
    Servy.PledgeServer,
    {Servy.SensorServer, interval: 60, target: "bigfoot"}
  ]

Then in the SensorServer.start_link function you could extract the options from the keyword list like so:

  def start_link(options) do
    interval = Keyword.get(options, :interval)
    target = Keyword.get(options, :target)
    ...
  end

Finally, for more fine-grained control, you can also specify the child as a map containing at least the :id and :start
fields of the child specification. For example:

  children = [
    Servy.PledgeServer,
    %{
      id: Servy.SensorServer,
      start: {Servy.SensorServer, :start_link, [60]}
    }
  ]


Overriding Child Spec Fields

If you need to override just a couple fields of the default child specification, you can list the overrides when using
the GenServer behavior.

For example, the following changes the :restart field from :permanent (the default) to :temporary:

  use GenServer, restart: :temporary

Suppose you also wanted to override how the child is started. Here's how to do that:

  use GenServer, start: {__MODULE__, :start_link, [60]}, restart: :temporary

And for the ultimate flexibility, you can override the default child_spec function by defining your own in the child
module. For example:

  def child_spec(arg) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [arg]},
      restart: :permanent,
      shutdown: 5000,
      type: :worker
    }
  end

Notice that you can choose to use arg or ignore it altogether. It's your call.

Multiple Child Specs

Being able to define your own child_spec function opens the door to making it a multi-clause function. You can use the
argument passed to child_spec as a pattern to match one of several function clauses, with each clause returning a custom
child specification.

For example, suppose we have the following function clauses in the SensorServer module:

   def child_spec(:frequent) do
     %{
       id: __MODULE__,
       start: {__MODULE__, :start_link, [1]},
       restart: :permanent,
       shutdown: 5000,
       type: :worker
     }
   end

   def child_spec(:infrequent) do
     %{
       id: __MODULE__,
       start: {__MODULE__, :start_link, [60]},
       restart: :permanent,
       shutdown: 5000,
       type: :worker
     }
   end

   def child_spec(_) do
     %{
       id: __MODULE__,
       start: {__MODULE__, :start_link, []},
       restart: :permanent,
       shutdown: 5000,
       type: :worker
     }
   end

The "frequent" clause indicates that the child should be started with a 1-minute refresh interval, whereas the
"infrequent" clause stipulates a 60-minute refresh interval. And the catch-all clause doesn't indicate a refresh interval.

You can then set up the supervisor with a SensorServer that has a frequent refresh interval using:

  children = [
    Servy.PledgeServer,
    {Servy.SensorServer, :frequent}
  ]

Or perhaps you'd rather start it with an infrequent refresh interval:

  children = [
    Servy.PledgeServer,
    {Servy.SensorServer, :infrequent}
  ]

Notice that the argument, either :frequent or :infrequent, is matched to run the appropriate child_spec function clause.


Child Specification Fields

Here's a quick break-down of the fields in the child specification and what they control:

  :id

  A name used internally by the supervisor to uniquely identify the child specification. This key is always required.

  Default: __MODULE__


  :start

  A {module, function, args} tuple used to start a child process. This key is always required.

  Default: {__MODULE__, :start_link, [[]]}


  :restart

  An atom that defines when a terminated child process should be restarted by its supervisor:

  :permanent indicates that the child process is always restarted

  :temporary indicates that the child process is never restarted

  :transient indicates that the child process is restarted only if it terminates abnormally. That is, the exit signal reason must be something other than :normal, :shutdown, or {:shutdown, term}.

  Default: :permanent


  :shutdown

  An atom that defines how a child process should be terminated by its supervisor:

  :brutal_kill indicates that the child process is brutally terminated using Process.exit(child_pid, :kill)

  :infinity indicates that the supervisor should wait indefinitely for the child process to terminate

  any integer indicates the amount of time in milliseconds that the supervisor will wait for a child process to
  terminate gracefully after sending it a polite Process.exit(child, :shutdown) signal. If no exit signal is received
  from the child process within the specified time, the child process is brutally terminated using
  Process.exit(child_pid, :kill). So the supervisor asks nicely once, then it drops the hammer.

  Default: 5000 if the type is :worker or :infinity if the type is :supervisor.


  :type

  An atom that indicates if the child process is a :worker or a :supervisor.

  Default: :worker


Restart Strategies and Other Supervisor Options

In the video, we initialized both supervisors with the :one_for_one restart strategy, like so

  Supervisor.init(children, strategy: :one_for_one)

Here's a quick summary of all the restart strategies:

  :one_for_one means if a child process terminates, only that process is restarted. None of the other child processes
  being supervised are affected.

  :one_for_all means if any child process terminates, the supervisor will terminate all the other children. All the child
  processes are then restarted. You'll typically use this strategy if all the child processes depend on each other and
  should therefore have their fates tied together.

  :rest_for_one means if a child process terminates, the rest of the child processes that were started after it
  (those children listed after the terminated child) are terminated as well. The terminated child process and the rest
  of the child processes are then restarted.

  :simple_one_for_one is intended for situations where you need to dynamically attach children. This strategy is
  restricted to cases when the supervisor only has one child specification. Using this specification as a recipe, the
  supervisor can dynamically spawn multiple child processes that are then attached to the supervisor.
  You would use this strategy if you needed to create a pool of similar worker processes, for example.

  In addition to the strategy option, you can also initialize a supervisor with the following other options:

    :max_restarts indicates the maximum number of restarts allowed within a specific time frame. After this threshold is
    met, the supervisor gives up. This is used to prevent infinite restarts. The default is 3 restarts.

    :max_seconds indicates the time frame in which :max_restarts applies. The default is 5 seconds.


Here's an example of how you'd set all the supervisor options, overriding the defaults:

  opts = [strategy: :one_for_one, max_restarts: 5, max_seconds: 10]

  Supervisor.init(children, opts)

Module-less Supervisors

In the video we defined both supervisors in their own modules that use the Supervisor behavior. It's worth noting that
you can create "module-less" supervisors. For example if we didn't want to define a ServicesSupervisor module, here's
how to create a similar supervisor "on the fly":

  children = [
    Servy.PledgeServer,
    {Servy.SensorServer, 60}
  ]

  {:ok, pid} = Supervisor.start_link(children, strategy: :one_for_one)

Notice that rather than calling Supervisor.init with the children and options, you need to call Supervisor.start_link instead.

We tend to prefer using a module because it keeps all the supervisor code organized.


Exercise: Configure the SensorServer

In a previous exercise you may have spiffed up the SensorServer to allow the refresh interval to be changed dynamically.
If you completed that exercise, then you have a State struct that looks like this:

  defmodule State do
    defstruct sensor_data: %{},
              refresh_interval: :timer.minutes(60)
  end

And your SensorServer.start function currently uses %State{} as the initial state:

  def start do
    IO.puts "Starting the sensor server..."
    GenServer.start(__MODULE__, %State{}, name: @name)
  end

In the video we showed how to configure the SensorServer such that a refresh interval is set when the process starts. To
do that, in the ServicesSupervisor we used a two-element tuple when describing the SensorServer child where the second
element of the tuple is a number indicating the refresh interval:

  children = [
    Servy.PledgeServer,
    {Servy.SensorServer, 60}
  ]

Then we changed the SensorServer.start_link function to take the refresh interval as the argument and we simply printed
it out to the screen:

  def start_link(interval) do
    IO.puts "Starting the sensor server with #{interval} min refresh..."
    GenServer.start_link(__MODULE__, %{}, name: @name)
  end

The point was to demonstrate how to configure a child process started by a supervisor. We didn't bother to do anything
with the refresh interval beyond printing it because, well, there wasn't anything new to learn by actually making it work.

So now it's your turn: Use the refresh interval passed to the start_link function to actually change the interval used
by the process!

  def start_link(interval) do
    IO.puts "Starting the sensor server with #{interval} min refresh..."
    initial_state = %State{refresh_interval: interval}
    GenServer.start_link(__MODULE__, initial_state, name: @name)
  end


Exercise: Supervise the FourOhFourCounter

In a previous exercise you converted a hand-rolled FourOhFourCounter server process to use the GenServer behavior.
Now that it's a GenServer, it can be properly supervised!

For that to work, you'll need to change FourOhFourCounter slightly to follow the expected conventions for starting a
linked process.

  def start_link(_arg) do

    IO.puts "Starting the 404 counter..."

    GenServer.start_link(__MODULE__, %{}, name: @name)

  end

Then add it as a child of a supervisor. You can either add it to the existing ServicesSupervisor or create an entirely
new supervisor and add that supervisor to the supervision tree.

  # In ServicesSupervisor, for example:

  children = [
    Servy.PledgeServer,
    {Servy.SensorServer, 60},
    Servy.FourOhFourCounter
  ]

Code So Far

The code for this video is in the 'supervisors' directory found within the video-code directory of the code bundle.

